Project Title : OverTime System

Description :
This is a Spring MVC project with Thymeleaf. The system has department, users, project and overtime details and masters.
The system will be used by the usersto maintain their overtime work record. The hours work will be recorded with the proper log and project they are working on and can be submitted to team lead on end of the month.

Installation and versions:
IntelliJ IDEA 2022.3.2 (Ultimate Edition)
Java - Ver 8
postgres (PostgreSQL) - Ver 15.1
Spring boot - Ver 3.0.5

Database Name: overtime_system
Port: 8090
