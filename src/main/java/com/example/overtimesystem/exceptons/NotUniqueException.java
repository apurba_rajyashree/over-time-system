package com.example.overtimesystem.exceptons;

public class NotUniqueException extends Exception{
    public NotUniqueException(String message) {
        super(message);
    }
}
