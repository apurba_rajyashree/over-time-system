package com.example.overtimesystem.dto;

import com.example.overtimesystem.entity.Project;
import com.example.overtimesystem.entity.ProjectMember;
import com.example.overtimesystem.entity.User;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ProjectMemberDto {
    private int id;
    private boolean isLead=false;
    private User user;
    private Project project;

    public ProjectMemberDto(ProjectMember projectMember) {
        this.id = projectMember.getId();
        this.user = projectMember.getUser();
        this.project = projectMember.getProject();
        this.isLead = projectMember.isLead();
    }
}
