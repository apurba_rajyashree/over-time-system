package com.example.overtimesystem.controller;

import com.example.overtimesystem.entity.OverTimeMaster;
import com.example.overtimesystem.helper.ExportOverTimeDetail;
import com.example.overtimesystem.repository.OverTimeDetailRepository;
import com.example.overtimesystem.repository.OverTimeMasterRepository;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.security.Principal;

@Controller
@RequiredArgsConstructor
public class ExportOverTimeController {

    private final OverTimeMasterRepository overTimeMasterRepository;

    private final OverTimeDetailRepository overTimeDetailRepository;

    @GetMapping("/master/export-detail/{detail_id}")
    public ResponseEntity<InputStreamResource> exportDetailReport(@PathVariable("detail_id") int id, Model model, Principal principal,
                                                                  RedirectAttributes redirectAttributes, HttpServletResponse response) throws IOException {
        OverTimeMaster overTimeMaster=overTimeMasterRepository.findById(id).orElseThrow(
                ()->new RuntimeException("OverTime Master does not exist!")
        );
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "attachment; filename=overtime.xlsx");


        ByteArrayInputStream in = ExportOverTimeDetail.OtdSheet(overTimeMaster.getOverTimeDetails(),overTimeDetailRepository,overTimeMaster);

        return ResponseEntity
                .ok()
                .headers(headers)
                .body(new InputStreamResource(in));
    }

}
